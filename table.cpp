#include "table.h"
using namespace std;

/*
   The purpose of this table.cpp file is to implement the table functions.
   There is a default constructor and destructor, a hash function to calculate
   the index, insert function insert into the hash table, load function to load
   infomation in from an external file to the hash table, display functions to 
   display the data, retrieve function to retirve certain data, and remove
   function to remove data based off name
 */

//constructor
table::table(int size)
{
	hash_table = new node * [size];
	table_size = size;
	for (int i = 0; i < table_size; ++i)
	{
		hash_table[i] = nullptr;
	}	
	//table_size = SIZE;
	table_size = size;
}

//destructor
table::~table()
{
	for (int i = 0; i < table_size; ++i)
	{
		node * current = hash_table[i];
		while(current)
		{
			node * temp = current;
			current = current->next;
			delete [] temp->data.name;
			delete [] temp->data.state;
			delete [] temp->data.rec;
			delete [] temp->data.time;
			delete [] temp->data.travel;
			delete [] temp->data.notes;
			delete temp;
		}
	}
	delete [] hash_table;
}

// calculate the index for a given key
int table::hash_function(char* key) const
{
	int num = 0;
	int length = strlen(key);
	for (int i = 0; i < length; ++i)
	{
		num += key[i];
	}
	return (num % table_size);
}

int table::hash_function2(char* key)const
{
	int num = 0;
	int length = strlen(key);
	for (int i = 0; i < length; ++i)
	{
		num = (num * + key[i]) % table_size;
	}
	return num;
}

// insert infomation
int table::insert(char * key_value, const infomation & to_add)
{
	int index = hash_function(key_value);
	//if(!hash_table[index])
	//	return 0;
	node * newNode = new node;
	newNode->data.name = new char[strlen(to_add.name) + 1];
	strcpy(newNode->data.name, to_add.name);
	newNode->data.state = new char[strlen(to_add.state) + 1];
	strcpy(newNode->data.state, to_add.state);
	newNode->data.rec = new char[strlen(to_add.rec) + 1];
	strcpy(newNode->data.rec, to_add.rec);
	newNode->data.time = new char[strlen(to_add.time) + 1];
	strcpy(newNode->data.time, to_add.time);
	newNode->data.travel = new char[strlen(to_add.travel) + 1];
	strcpy(newNode->data.travel, to_add.travel);
	newNode->data.notes = new char[strlen(to_add.notes) + 1];
	strcpy(newNode->data.notes, to_add.notes);
	newNode->next = nullptr;

	if(hash_table[index] == nullptr)
		hash_table[index] = newNode;
	else
	{	
		node * current = hash_table[index];
		while(current->next != nullptr)
		{
			current = current->next;
		}
		current->next = newNode;
	}
	return index;
}

int table::save(char * file)
{
	ofstream file_out(file, ios::app);
	if (!file_out)
		return 0;
	for (int i = 0; i < table_size; ++i)
	{
		node * current = hash_table[i];
		while(current != nullptr)
		{
			file_out << current->data.name << " | "
				<< current->data.state << " | "
				<< current->data.rec << " | "
				<< current->data.time << " | "
				<< current->data.travel << " | "
				<< current->data.notes << endl;
			current = current->next;
		}
	}
	file_out.close();
	return 1;

}


// load external file
int table::load(char * file)
{
	ifstream file_in(file);
	if (!file_in)
		return 0;
	//file_in.open("program3.txt");
	int count = 0;
	char name[NUM];
	char state[NUM];
	char rec[NUM];
	char time[NUM];
	char travel [NUM];
	char notes[NUM];
	infomation Info;
	while (file_in && !file_in.eof() && count < table_size)
	{
		file_in.get(name, NUM, '|');
		file_in.ignore(100, '|');
		Info.name = new char[strlen(name) + 1];
		strcpy(Info.name, name);

		file_in.get(state, NUM, '|');
		file_in.ignore(100, '|');
		Info.state = new char[strlen(state) + 1];
		strcpy(Info.state, state);

		file_in.get(rec, NUM, '|');
		file_in.ignore(100, '|');
		Info.rec = new char[strlen(rec) + 1];
		strcpy(Info.rec, rec);

		file_in.get(time, NUM, '|');
		file_in.ignore(100, '|');
		Info.time = new char[strlen(time) + 1];
		strcpy(Info.time, time);

		file_in.get(travel, NUM, '|');
		file_in.ignore(100, '|');
		Info.travel = new char[strlen(travel) + 1];
		strcpy(Info.travel, travel);

		file_in.get(notes, NUM, '\n');
		file_in.ignore(100, '\n');
		Info.notes = new char[strlen(notes) + 1];
		strcpy(Info.notes, notes);

		insert(Info.name, Info);
		++count;
		/*Info.name = new char[strlen(name) + 1];
		  strcpy(Info.name, name);
		  file_in.ignore(100, '|');
		  Info.name = new char[strlen(state) + 1];
		  strcpy(Info.state, state);
		  file_in.ignore(100, '|');
		  Info.name = new char[strlen(rec) + 1];
		  strcpy(Info.rec, rec);
		  file_in.ignore(100, '|');
		  Info.name = new char[strlen(time) + 1];
		  strcpy(Info.time, time);
		  file_in.ignore(100, '|');
		  Info.name = new char[strlen(travel) + 1];
		  strcpy(Info.travel, travel);
		  file_in.ignore(100, '|');
		  Info.name = new char[strlen(notes) + 1];
		  strcpy(Info.notes, notes);
		  file_in.ignore(100, '|');
		  insert(Info.name, Info);
		  ++count;
		 */
	}
	file_in.close();
	return count;
}

// displays all data
int table::display_all()
{
	for (int i = 0; i < table_size; ++i)
	{
		node * current = hash_table[i];
		while(current != nullptr)
		{
			cout << current->data.name << " | " << current->data.state << " | "
				<< current->data.rec << " | " << current->data.time << " | "
				<< current->data.travel << " | " << current->data.notes << endl;
			current = current->next;
		}
	}
	return 1;
}

// displays a match
int table::display_match(const char * to_find)
{
	//int index = hash_function(to_find);
	//node * current = hash_table[index];
	int match = 0;
	for (int i = 0; i < table_size; ++i)
	{
		node * current = hash_table[i];
		while(current != nullptr)
		{
			if(strcmp(current->data.name, to_find) == 0)
			{			
				cout << current->data.name << " | " << current->data.state << " | "
					<< current->data.rec << " | " << current->data.time << " | "
					<< current->data.travel << " | " << current->data.notes << endl;
				++match;
			}
			current = current->next;
		}
	}
	return match; //if match 0 no matches found
}

//retrieve an entry from the user and give back to client
int table::retrieve(char * to_find, infomation & found)
{
	int index = hash_function(to_find);
	node * current = hash_table[index];
	int match = 0;
	while(current != nullptr)
	{
		if(strcmp(current->data.name, to_find) == 0)
		{
			found = current->data;
			++ match;
		}
		current = current->next;
	}
	return match;
}

// remove an entry from the file
int table::remove(char * to_remove)
{
	int index = hash_function(to_remove);
	node * current = hash_table[index];
	int match = 0;
	while(current != nullptr)
	{
		if(strcmp(current->data.name, to_remove) == 0)
		{
			if (current == hash_table[index])
				hash_table[index] = current->next;
			else
			{
				node* temp = hash_table[index];
				while (temp->next != current)
					temp = temp->next;
				temp->next = current->next;
			}	
			delete [] current->data.name;
			delete [] current->data.state;
			delete [] current->data.rec;
			delete [] current->data.time;
			delete [] current->data.travel;
			delete [] current->data.notes;
			delete current;
			++match;
		}
		current = current->next;
	}
	return match;
}

// update after a delete
int table::updateSave(char * file)
{
	ofstream file_out(file, ios::trunc);
	if (!file_out)
		return 0;
	for (int i = 0; i < table_size; ++i)
	{
		node * current = hash_table[i];
		while(current != nullptr)
		{
			file_out << current->data.name << " | "
				<< current->data.state << " | "
				<< current->data.rec << " | "
				<< current->data.time << " | "
				<< current->data.travel << " | "
				<< current->data.notes << endl;
			current = current->next;
		}
	}
	file_out.close();
	return 1;
}

int table::displayAttraction( const char * attraction)
{
	return displayAttraction(attraction, 0);
}
/*
Task 9. Display all by another field of your choice (e.g., maybe find all 
matches for a particular attraction (where are all of the Disneylands 
in the world?)
a. This will require a search through the entire data structure

*/
int table::displayAttraction(const char * attraction, int index)
{
	if(index >= table_size)
		return 0;
	node * current = hash_table[index];
	if(current != nullptr)
	{
		if(strcmp(current->data.rec, attraction) == 0)
		{
			cout << current->data.name << " | "
				<< current->data.state << " | "
				<< current->data.rec << " | "
				<< current->data.time << " | "
				<< current->data.travel << " | "
				<< current->data.notes << endl;
		}
		current = current->next;
	}
	return displayAttraction(attraction, index + 1);
}
