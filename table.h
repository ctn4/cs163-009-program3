#include <cstring>
#include <cctype>
#include <iostream>
#include <fstream>

//Christine Nguyen, CS163, Program3, 05-09-23
//The purpose of this table header file is to use hash tables
//to retrieve and store data using a key value. Loading in external
//files based on name and insert, remove, and display the data within
//these external files

#ifndef TABLE
#define TABLE
const int NUM = 100;
//const int SIZE = 23;

//infomation of a event
struct infomation
{
	char * name;	//Name of the location
	char * state;	//State and/or Country
	char * rec;	//Best things to do there
	char * time;	//Best time of the year to go
	char * travel;	//How to travel to there
	char * notes;	//Any notes the user has about the trip
};

//node for the hash table
struct node
{
	infomation data;
	node * next;
};

//class for has table
class table
{
	public:
		table(int size);	//default constructor
		~table();	//destructor
		//int hash_function(char * key) const;
		int insert(char * key_value, const infomation & to_add);
		int load(char * file);
		int display_all();
		int display_match(const char * to_find);
		int retrieve(char * to_find, infomation & found);
		int remove(char * to_remove);
		int save(char * file);
		int updateSave(char * file);
		int displayAttraction( const char * attraction);
		int displayAttraction(const char * attraction, int index);
	private:
		node ** hash_table;
		int table_size;
		int hash_function(char * key) const;
		int hash_function2(char * key) const;	//prime number
};
#endif
