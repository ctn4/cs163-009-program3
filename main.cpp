#include "table.h"
using namespace std;

/*
	This main.cpp is to call and test the functions in
	table.h.
*/

int main()
{
	table * myTable;
	infomation myInfo;
	infomation found;
	int size {0};
	int choice {0};
	ifstream file_in;
	ofstream file_out;
	char file[NUM];
	char to_find[NUM];
	char to_remove[NUM];
	char attraction[NUM];
	int result {0};


	cout << "Enter size of hash table: ";
	cin >> size;
	cin.ignore(100, '\n');
	myTable = new table(size);

	do
	{		
		cout << "\n*********MENU*********" << endl
			<< "[1] Add a location" << endl
			<< "[2] Load data from file" << endl
			<< "[3] Display all" << endl
			<< "[4] Display location by name" << endl
			<< "[5] Retrieve location by name" << endl
			<< "[6] Remove location by name" << endl
			<< "[7] Display a particular attraction" << endl
			<< "[8] Exit" << endl << endl;
		cout << "Enter Choice: ";
		cin >> choice;
		cin.ignore(100, '\n');	
		if(choice == 1)
		{
			cout << "\nEnter the name of the location: ";
			myInfo.name = new char[NUM];
			cin.get(myInfo.name, NUM, '\n');
			cin.ignore(100,'\n');
			cout << "Enter the state and/or country: ";
			myInfo.state = new char[NUM];
			cin.get(myInfo.state, NUM, '\n');
			cin.ignore(100,'\n');
			cout << "Enter the best things to do there: ";
			myInfo.rec = new char[NUM];
			cin.get(myInfo.rec, NUM, '\n');
			cin.ignore(100,'\n');
			cout << "Enter the best time of the year to go: ";
			myInfo.time = new char[NUM];
			cin.get(myInfo.time, NUM, '\n');
			cin.ignore(100,'\n');
			cout << "Enter how to travel there: ";
			myInfo.travel = new char[NUM];
			cin.get(myInfo.travel, NUM, '\n');
			cin.ignore(100,'\n');
			cout << "Enter any notes about the trip: ";
			myInfo.notes = new char[NUM];
			cin.get(myInfo.notes, NUM, '\n');
			cin.ignore(100,'\n');

			myTable->insert(myInfo.name, myInfo);
			myTable->display_all();			

			cout << "Enter the file you would like to save to: ";
			cin.get(file, NUM, '\n');
			cin.ignore(100, '\n');
			result = myTable->save(file);
			if(result == 0)
				cout << "Failed to save" << endl;
			else if (result == 1)
				cout << "Successfully saved" << endl;
			
		}	
		if(choice == 2)
		{
			cout << "Enter the filename: ";
			cin.get(file, NUM, '\n');
			cin.ignore(100, '\n');
			myTable->load(file);
		}
		if(choice == 3)
		{
			myTable->display_all();	
		}
		if(choice == 4)
		{
			cout << "\nEnter the name of the location you want to display: ";
			cin.get(to_find, NUM, '\n');
			cin.ignore(100, '\n');
			result = myTable->display_match(to_find);
			if(result == 0)
				cout << "Location not found." << endl;
		}	
		if(choice == 5)
		{
			cout << "\nEnter the name of the location you want to retrieve: ";
			cin.get(to_find, NUM, '\n');
			cin.ignore(100, '\n');
			result = myTable->retrieve(to_find, found);
			if (result != 0)
			{	
				cout << found.name << " | " << found.state << " | "
				     << found.rec << " | " << found.time << " | "
				     << found.travel << " | " << found.notes << endl;
			}
			else if(result == 0)
				cout << "Location not found." << endl;
		}
		if(choice == 6)
		{
			cout << "\nEnter the name of the location you want to remove: ";
			cin.get(to_remove, NUM, '\n');
			cin.ignore(100, '\n');
			result = myTable->remove(to_remove);
			if(result == 0)
				cout << "Removed failed" << endl;
			else if(result != 0)
				cout << "Removed Successfull" << endl;
			result = myTable->updateSave(file);	//save remove
			if(result == 0)
				cout << "File not updated" << endl;
			else if(result != 0)
				cout << "File Updated with Removal" << endl;
		}
		if(choice == 7)
		{
			cout << "\nEnter the name of the attraction you want to display: ";
			cin.get(attraction, NUM, '\n');
			cin.ignore(100, '\n');
			myTable->displayAttraction(attraction);
		}
			
	}while(choice != 8);
	return 0;
}
